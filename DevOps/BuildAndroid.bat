echo ========== BUILDING ANDROID ==========

set ueVersion=4.26
set pathToPlugin="D:\Projects\UE4\TranslateOverview\Plugins\TextTranslation\TextTranslation.uplugin"
set buildPath="D:\Projects\UE4\TranslateOverview\Build\Android"
set pathToRunUAT="D:\Programm\UnrealEngine\UE_%ueVersion%\Engine\Build\BatchFiles\RunUAT.bat"

call %pathToRunUAT% BuildPlugin -plugin=%pathToPlugin% -package=%buildPath% -TargetPlatforms=Android -VS2019

pause

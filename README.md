Text Translation is a plugin that makes it easy to use [Unreal Engine 4](https://www.unrealengine.com/) with [Google Translate](https://cloud.google.com/translate) service via REST requests.

[Plugin in Unreal Engine Marketplace](https://www.unrealengine.com/marketplace/en-US/profile/Hichigo)

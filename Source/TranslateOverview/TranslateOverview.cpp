// Copyright Epic Games, Inc. All Rights Reserved.

#include "TranslateOverview.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TranslateOverview, "TranslateOverview" );
